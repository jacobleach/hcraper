module Main where

import Database.PostgreSQL.Simple (Connection, connectPostgreSQL, executeMany)
import System.Environment (getArgs)
import Text.HTML.DOM (parseLBS)

import Database.Forum (insertForumListings)
import Mine (fetchIndex)
import Parse.Forum (ForumIndex(..), forumsOnIndex)

getDatabaseConnection :: IO (Connection)
getDatabaseConnection = connectPostgreSQL "postgresql://"

main :: IO ()
main = do
  args <- getArgs
  forumHtml <- fetchIndex (args !! 0)
  connection <- getDatabaseConnection
  let parsedHtml = (ForumIndex . parseLBS) forumHtml
  case forumsOnIndex parsedHtml of
    Left a -> print a
    Right x -> do
      insertForumListings connection x
      putStrLn "success"
  return ()
