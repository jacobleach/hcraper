module Mine
  (
    fetchForum
  , fetchIndex
  ) where

import Data.ByteString.Lazy (ByteString)
import Network.Wreq (get, responseBody)
import Control.Lens ((^.))

fetchIndex :: String -> IO (ByteString)
fetchIndex url = do
  response <- get $ url
  return $ response ^. responseBody

fetchForum :: String -> String -> IO (ByteString)
fetchForum url id = do
  response <- get $ url ++ "/forums/" ++ id
  return $ response ^. responseBody
