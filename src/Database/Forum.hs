module Database.Forum
  (
    insertForumListings
  , insertForumListing
  ) where

import Data.Int (Int64)
import Database.PostgreSQL.Simple (Connection, Query, execute, executeMany)
import Database.PostgreSQL.Simple.ToRow (ToRow (toRow))
import Database.PostgreSQL.Simple.ToField (ToField (toField))

import qualified Parse.Forum as Forum (ForumListing (..))

instance ToRow Forum.ForumListing where
  toRow Forum.ForumListing{..} = [toField id, toField name, toField description]

instance ToField Forum.ForumListing where
  toField Forum.ForumListing{..} = toField id

insertForumListingQuery :: Query
insertForumListingQuery =
  "INSERT INTO\
 \ forums (id, name, description) VALUES (?, ?, ?)\
 \ ON CONFLICT (id) DO UPDATE SET name = EXCLUDED.name, description = EXCLUDED.description"

insertForumListing :: Connection -> Forum.ForumListing -> IO (Int64)
insertForumListing connection forumListing = execute connection insertForumListingQuery forumListing

insertForumListings :: Connection -> [Forum.ForumListing] -> IO (Int64)
insertForumListings connection forumListing = executeMany connection insertForumListingQuery forumListing
