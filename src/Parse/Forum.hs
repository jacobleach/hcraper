module Parse.Forum
  (
    ForumIndex(..)
  , ForumListing(..)
  , forumsOnIndex
  ) where

import Control.Monad.Except (MonadError, throwError)
import qualified Data.Map as M (Map, lookup)
import qualified Data.Text as T (Text, filter, isPrefixOf, length, splitAt, unpack, words)
import Prelude hiding (id)
import Text.Read (readMaybe)
import Text.XML (Document, Name)
import Text.XML.Cursor (($//), (>=>), Axis, Cursor, attributeIs, content, fromDocument, node)

import Parse.Utils
  (
    ParseError(..)
  , attributeHasWord
  , blockquoteNode
  , getAttributesFromNode
  , getClassAttribute
  , listItemNode
  )

newtype ForumIndex = ForumIndex { document :: Document } deriving (Show)
newtype ForumIndexListing = ForumIndexListing { cursor :: Cursor }

data ForumListing = ForumListing { id :: Int, name :: T.Text, description :: T.Text } deriving (Show)

forumsOnIndex :: (MonadError ParseError m) => ForumIndex -> m [ForumListing]
forumsOnIndex ForumIndex{document} = do
  let matchingCursors = (fromDocument document) $// getAllForumClassNodes
  let matchingNodes = map (node . cursor) matchingCursors
  attributes <- mapM getAttributesFromNode matchingNodes
  classContent <- mapM getClassAttribute attributes
  ids <- mapM getIdFromListNodeClass classContent
  names <- mapM getForumTitle matchingCursors
  descriptions <- mapM getForumDescription matchingCursors
  return (map (\(id, name, description)-> ForumListing id name description) (zip3 ids names descriptions))

getAllForumClassNodes :: Cursor -> [ForumIndexListing]
getAllForumClassNodes cursor = ForumIndexListing <$> (listItemNode >=> attributeHasWord "class" "forum") cursor

getForumTitle :: (MonadError ParseError m) => ForumIndexListing -> m T.Text
getForumTitle ForumIndexListing{cursor} = case cursor $// attributeIs "class" "nodeTitle" of
  [] -> throwError $ GenericParseError "`getForumTitle` unable to find node with `nodeTitle` class"
  x:_ -> case x $// content of
    [] -> throwError $ GenericParseError "`getForumTitle` unable to find forum title"
    -- There will be more than one if there is someone viewing the forum (`(1 viewing)` appears)
    (x:xs) -> return (T.filter (/= '\n') x)

getForumDescription :: (MonadError ParseError m) => ForumIndexListing -> m T.Text
getForumDescription ForumIndexListing{cursor} =
  case cursor $// blockquoteNode >=> attributeHasWord "class" "nodeDescription" of
    [] -> throwError $ GenericParseError "`getForumDescription` unable to find blockquote with node description"
    -- TODO: Do not just take first element
    x:_ -> case x $// content of
      [] -> throwError $ GenericParseError "`getForumDescription` unable to find forum description"
      (x:[]) -> return (T.filter (/='\n') x)
      (x:xs) -> throwError $ GenericParseError "`getForumDescription` found multiple items"

getIdFromListNodeClass :: (MonadError ParseError m) => T.Text -> m Int
getIdFromListNodeClass value = do
  classValue <- getNodeIdTextFromClassList value
  parseIdFromListNodeClass classValue

parseIdFromListNodeClass :: (MonadError ParseError m) => T.Text -> m Int
parseIdFromListNodeClass value = do
  let idAsText = (snd . T.splitAt (T.length listNodeIdClassPrefix)) value
  case readMaybe (T.unpack idAsText) of
    Nothing -> throwError $ ForumIdFromListNodeClassWrong $ "Failed to parse: '" ++ (T.unpack value) ++ "' into Int"
    Just x -> return x

getNodeIdTextFromClassList :: (MonadError ParseError m) => T.Text -> m T.Text
getNodeIdTextFromClassList value = do
  let matchingWords = filter (T.isPrefixOf listNodeIdClassPrefix) (T.words value)
  case matchingWords of
    x:[] -> return x
    x:xs -> throwError $ MultipleForumIdsInList $
      "Found multiple values in class attribute that match '" ++ (T.unpack listNodeIdClassPrefix) ++ "'"

listNodeIdClassPrefix :: T.Text
listNodeIdClassPrefix = "node_"
